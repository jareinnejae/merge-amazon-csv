import React, { Component, Fragment } from "react";
import scrapeAmazonAsin from "./AmazonScraper";
import csv from "jquery-csv";
import Button from 'react-bootstrap/Button'
import Spinner from 'react-bootstrap/Spinner'

const pLimit = require('p-limit');

class MergeAmazonCSV extends Component {
    constructor(props) {
        super(props);

        this.state = {
            filteredAmazonCSV: null,
            detailPageSalesCSV: null,
            skagCSV: null,
            newlyScrapedData: [],
            result: [],
            isMergingFiles: false,
            isScraping: false,
            hasMergedCSV: false,
            asins: "",
            asinsList: [],
            scrapedAsin: 0
        };
    }

    componentDidMount() {
    }

    getFile = (name) => (e) => {
        let _this = this;
        let file = e.target.files[0];
        let reader = new FileReader();
        reader.readAsText(file);

        reader.onload = function (event) {
            let _csv = event.target.result;
            let data = csv.toArrays(_csv);

            _this.state[name] = data;
        };
    };

    setHeaders = () => {
        let _this = this;
        let tempFilteredAmazonCSVHeaders = this.state.filteredAmazonCSV[0];
        _this.state["detailPageSalesCSVHeaders"] = this.state.detailPageSalesCSV[0];

        for (let i = 0; i < 13; i++) {
            tempFilteredAmazonCSVHeaders.push(this.state.detailPageSalesCSV[0][i+4]);
        }
        _this.state["filteredAmazonCSVHeaders"] = tempFilteredAmazonCSVHeaders;
    }

    removeHeaders = () => {
        if (this.state.filteredAmazonCSV) this.state.filteredAmazonCSV.shift();
        if (this.state.detailPageSalesCSV) this.state.detailPageSalesCSV.shift();
        if (this.state.skagCSV) this.state.skagCSV.shift();
    };

    startMergingProcess = (prohibitDownload = false) => {
        this.setHeaders();
        this.removeHeaders();
        this.setState({
            isMergingFiles: true,
            hasMergedCSV: false
        });
        let fixedDetailPageSalesCSV = this.fixDetailPageSalesCSV();
        let fixedSkagCSV = this.fixSkagCSV();
        let mergedCSV = this.mergeCSVs(fixedDetailPageSalesCSV, fixedSkagCSV);
        if(!prohibitDownload) {
            this.saveToCSV(mergedCSV);
        }

        return mergedCSV;
    };
    
    fixDetailPageSalesCSV = () => {
        let regex = /(\d*\.?\d+|\d{1,3}(,\d{3})*(\.\d+)?)/
        let subgroupCollection = {};
        let detailPageSalesTemp = this.state.detailPageSalesCSV;

        detailPageSalesTemp.forEach(function (elem) {
            if (Object.keys(subgroupCollection).indexOf(elem[1]) === -1) {
                subgroupCollection[elem[1]] = [];
            }
            subgroupCollection[elem[1]].push(elem);
        });
        
        for (const obj in subgroupCollection) {
            subgroupCollection[obj] = subgroupCollection[obj].sort(function(a, b) {
                return (a[1].toLowerCase() > b[1].toLowerCase()) ? 1 : ((b[1].toLowerCase() > a[1].toLowerCase()) ? -1 : 0)
            });
        }

        for (const obj in subgroupCollection) {
            let results = [];
            let skuHandled = [];
            subgroupCollection[obj].forEach(function(item, i) {
                if (skuHandled.indexOf(item[3]) === -1) {
                    let eQData = [];
                    let filteredData = subgroupCollection[obj].filter(function (el) {
                        return el[3] === item[3];
                    });
                    eQData[0] = item[0];
                    eQData[1] = item[1];
                    eQData[2] = item[2];
                    eQData[3] = item[3];
                    for (let index = 4; index < item.length; index++) {
                        let pluckedData = filteredData.map( function (el) { return el[index] });
                        if (pluckedData[0].match('%')) {
                            let tempResult = 0.0;
                            pluckedData.forEach(function (data) {
                                let regexMatchTemp = data.replace(',','').match(regex).map(function(v) { return parseFloat(v); });
                                tempResult += regexMatchTemp.length > 0 ? regexMatchTemp[0] : 0;
                            })
                            tempResult /= pluckedData.length;
                            eQData[index] = (tempResult/100).toFixed(4);
                        } else {
                            let tempResult = 0.0;
                            pluckedData.forEach(function (data) {
                                let regexMatchTemp = data.replace(',','').match(regex).map(function(v) { return parseFloat(v); });
                                tempResult += regexMatchTemp.length > 0 ? regexMatchTemp[0] : 0;
                            })
                            eQData[index] = tempResult;
                        }
                    }
                    results.push(eQData);
                    skuHandled.push(item[3]);
                }
            });
            subgroupCollection[obj] = results;
        }

        this.setState({
            detailPageSalesCSV: subgroupCollection
        });

        return subgroupCollection;
    };

    fixSkagCSV = () => {
        let subgroupCollection = {};
        let skagCSVTemp = this.state.skagCSV;

        skagCSVTemp.forEach(function (elem) {
            if (Object.keys(subgroupCollection).indexOf(elem[4]) === -1) {
                subgroupCollection[elem[4]] = [];
            }
            subgroupCollection[elem[4]].push(elem);
        });
        
        for (const obj in subgroupCollection) {
            subgroupCollection[obj] = subgroupCollection[obj].sort(function(a, b) {
                return (a[3].toLowerCase() > b[3].toLowerCase()) ? 1 : ((b[3].toLowerCase() > a[3].toLowerCase()) ? -1 : 0)
            });
        } 

        this.setState({
            skagCSV: subgroupCollection
        })
        return subgroupCollection;
    }

    mergeCSVs = (fixedDetailPageSalesCSV, fixedSkagCSV) => {
        let _this = this;
        let filteredAmazonCSVTemp = _this.state.filteredAmazonCSV;
        let header = _this.state.filteredAmazonCSVHeaders;
        header.push('SKAG Status');
        let originalLength = filteredAmazonCSVTemp[0].length;
        filteredAmazonCSVTemp.map(function(item) {
            let findDetailPageSale = Object.keys(fixedDetailPageSalesCSV).indexOf(item[5]) !== -1 ? fixedDetailPageSalesCSV[item[5]].filter(function(obj) {
                return obj[3] === item[2];
            }) : [];
            let findSkag = Object.keys(fixedSkagCSV).indexOf(item[5]) !== -1 ? fixedSkagCSV[item[5]].filter(function(obj) {
                return obj[3] === item[2];
            }) : [];
            for (let i = 0; i < 13; i++) {
                item[originalLength - 1 + i] = findDetailPageSale.length > 0 ? findDetailPageSale[0][i+4] : 0;
            }
            if (findSkag.length > 0) {
                item[item.length] = findSkag[0][2];
            }
            return item;
        });
        filteredAmazonCSVTemp.unshift(
            header
        );

        this.setState({
            filteredAmazonCSV: filteredAmazonCSVTemp
        })
        return filteredAmazonCSVTemp;
    }

    saveToCSV = (mergedCSV) => {
        this.setState({
            isMergingFiles: false,
            hasMergedCSV: true
        }, () => {
            this.finalizeMergedCSVFile(mergedCSV);
        });
    };

    finalizeMergedCSVFile = (saveFile) => {
        var rawFile = csv.fromArrays(saveFile);

        var link = document.createElement("a");
        link.href = URL.createObjectURL(
            new Blob([rawFile], {
                type: "text/csv",
            })
        );
        link.setAttribute("download", "my_data.csv");
        document.body.appendChild(link);
        link.click();
    };

    scrapeFromAmazon = () => {
        let { asins } = this.state;
        let _this = this;

        this.setState({
            isScraping: true,
            isProcessing: true,
            asinsList: asins.split(',')
        }, () => {
            _this.bulkFetchAsins(asins.split(',')).then((results) => {
                _this.setState({
                    isScraping: false,
                    isProcessing: false,
                    newlyScrapedData: results,
                    asinsList: [],
                    scrapedAsin: 0
                });
                results.unshift(['asin', 'brand_name', 'product_title', 'description', 'ratings', 'total_reviews', 'availability', 'bullet_1', 'bullet_2', 'bullet_3', 'bullet_4', 'bullet_5', 'bullet_6', 'bullet_7', 'bullet_8'])
                _this.finalizeMergedCSVFile(results)
            });
        });

    }

    bulkFetchAsins = (asins) => {
        let _this = this;
        return new Promise(function(resolve, reject) {
            let results = [];
            let promises = [];
            const limit = pLimit(2);
            for (let i = 0; i < asins.length; i++) {
                promises.push(limit(
                    () => scrapeAmazonAsin(asins[i]).then((result) => {
                        results.push(result[1]);
                        _this.setState({
                            scrapedAsin: results.length
                        })
                    }).catch(() => {
                        results.push([asins[i]]);
                    }))
                )
            }

            (async () => {
                // Only three promises are run at once (as defined above)
                await Promise.all(promises);
                resolve(results);
            })();
        });
    }

    mergeScrapedDataWithAmazon = async () => {;
        let _this = this;

        let filteredAmazonCSV = _this.startMergingProcess(true);
        let filteredAmazonCSVHeaders = filteredAmazonCSV[0];
        filteredAmazonCSV.shift();
        let asins = filteredAmazonCSV.map(function(item) {
            return item[5];
        });
        this.setState({
            isProcessing: true,
            asinsList: asins
        });

        _this.bulkFetchAsins(asins).then((results) => {
            let newlyScrapedDataHeaders = ['asin', 'brand_name', 'product_title', 'description', 'ratings', 'total_reviews', 'availability', 'bullet_1', 'bullet_2', 'bullet_3', 'bullet_4', 'bullet_5', 'bullet_6', 'bullet_7', 'bullet_8'];
            
            results.shift();

            filteredAmazonCSV.map(function(item) {
                let findNewlyScrapedObj = results.filter(obj => {
                    return obj[0] === item[5]
                });
                if (findNewlyScrapedObj.length > 0) {
                    for (let i = 1; i < newlyScrapedDataHeaders.length; i ++) {
                        item[filteredAmazonCSVHeaders.length + i - 1] = findNewlyScrapedObj[0][i];
                    }
                }
                return item;
            });

            for (let i = 1; i < newlyScrapedDataHeaders.length; i ++) {
                filteredAmazonCSVHeaders.push(newlyScrapedDataHeaders[i]);
            }

            filteredAmazonCSV.unshift(
                filteredAmazonCSVHeaders
            );
            _this.setState({
                isProcessing: false,
                scrapedAsin: 0,
                asinsList: []
            })
            _this.saveToCSV(filteredAmazonCSV);
        });

    }
    checkIfCanMerge = () => {
        return !!this.state.filteredAmazonCSV
    }
    
    changeAsins = (e) => {
        this.setState({
            asins: e.target.value
        });
    }

    render() {
        return (
            <div className="process-csv" style={{ textAlign: "left" }}>
                <div style={{ float: "left" }}>
                    <div>Select Amazon Seller Central CSV</div>
                    <input
                        type="file"
                        accept=".csv"
                        onChange={this.getFile("filteredAmazonCSV")}
                        style={{ float: "left" }}
                    />
                </div>
                <br />
                <br />
                <div style={{ float: "left" }}>
                    <div>Select Detail Page Sales and Traffic CSV</div>
                    <input
                        type="file"
                        accept=".csv"
                        onChange={this.getFile("detailPageSalesCSV")}
                        style={{ float: "left" }}
                    />
                </div>
                <br />
                <br />
                <div style={{ float: "left" }}>
                    <div>Select SKAG CSV</div>
                    <input
                        type="file"
                        accept=".csv"
                        onChange={this.getFile("skagCSV")}
                        style={{ float: "left" }}
                    />
                    <br />
                    <br />
                    <Button
                        style={{ float: "left" }}
                        variant="primary"
                        disabled={this.state.isMergingFiles}
                        onClick={ () => { this.startMergingProcess(false) } }
                    >
                        {
                            this.state.isMergingFiles ? 
                                <Fragment>
                                    <Spinner
                                        as="span"
                                        animation="grow"
                                        size="sm"
                                        role="status"
                                        aria-hidden="true"
                                    /> 
                                    Loading...
                                </Fragment> :
                                "Start Merging & Download CSV"
                        }
                    </Button>
                    <br />
                    <br />
                </div>
                {/* {
                    this.state.filteredAmazonCSV && this.state.detailPageSalesCSV && 
                        <Fragment>
                            <input type="text" onChange={e => this.changeAsins(e)} value={this.state.asins}/>
                            <button onClick={this.scrapeFromAmazon}>
                                Scrape Amazon
                            </button>
                        </Fragment>
                } */}
                <Fragment>
                    <div style={{float:"left", width:"100%"}}>
                        <input placeholder="Enter ASIN/s here" type="text" onChange={e => this.changeAsins(e)} value={this.state.asins}/>
                        <br />
                        <Button
                            variant="primary"
                            disabled={this.state.isScraping}
                            onClick={this.scrapeFromAmazon}
                        >
                            {
                                this.state.isScraping ? 
                                    <Fragment>
                                        <Spinner
                                            as="span"
                                            animation="grow"
                                            size="sm"
                                            role="status"
                                            aria-hidden="true"
                                        /> 
                                        Loading...
                                    </Fragment> :
                                    "Scrape Amazon"
                            }
                        </Button>
                    </div>
                    <br />
                    <br />
                    <div style={{float:"left", width: "100%"}}>
                        <input name="hasMergedCSV" type="checkbox" onChange={() => {
                            this.setState({hasMergedCSV: !this.state.hasMergedCSV})
                        }}/>
                        <label for="hasMergedCSV"> Toggle to directly merge Amazon CSV and Scraped data using the ASINS </label>
                        <div>
                            {
                                this.state.hasMergedCSV && 
                                <Button
                                    variant="info"
                                    disabled={!this.state.hasMergedCSV}
                                    onClick={this.mergeScrapedDataWithAmazon}
                                >
                                    Scrape Amazon and Merge With Final Skag CSV
                                </Button>
                            
                            }
                        </div>
                        {
                            this.state.isProcessing && 
                            <div>
                                <p>Done scraping {this.state.scrapedAsin} out of {this.state.asinsList.length}</p>
                            </div>
                        }
                    </div>
                </Fragment>
            </div>
        );
    }
}

export default MergeAmazonCSV;
