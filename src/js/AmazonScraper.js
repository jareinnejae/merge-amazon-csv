const scrapeAmazonAsin = async (asins) => {
    return new Promise (async (resolve, reject) => {
        try {
            console.log("Started scraping...");
            asins = asins.split(',');
            const regex = /[+-]?\d+(\.\d+)?/g;
            let results = [];
            let result = [];
            results.push(['asin', 'brand_name', 'product_title', 'description', 'ratings', 'total_reviews', 'availability', 'bullet_1', 'bullet_2', 'bullet_3', 'bullet_4', 'bullet_5', 'bullet_6', 'bullet_7', 'bullet_8']);
            for (let i = 0; i<asins.length; i++) {
                const response = await fetch(process.env.NODE_ENV === "production" ? `https://merge-amazon-csv-express.herokuapp.com?asin=${asins[i]}` : `http://localhost:7000?asin=${asins[i]}`);
                const body = await response.json();
                console.log(`Done scraping for asin=${asins[i]}`);
                result.push(asins[i]);
                body.brand_name = body.brand_name.replace(/(Visit|the|Store|Brand:)/gm, "").trim();
                for (var key of Object.keys(body)) {
                    if (key === "feature-bullets") {
                        for (let j = 0; j < body["feature-bullets"].length; j++) {
                            result.push(body["feature-bullets"][j]);
                        }
                    } else if (key === 'ratings') {
                        let tempArray = body["ratings"].trim().match(regex);
                        tempArray = tempArray ? tempArray.map(function(v) { return parseFloat(v); }) : [];
                        result.push(tempArray.length > 0 ? tempArray[0] : "");
                    } else if (key === 'total_reviews') {
                        let tempArray = body["total_reviews"].trim().match(regex);
                        tempArray = tempArray ? tempArray.map(function(v) { return parseFloat(v); }) : [];
                        result.push(tempArray.length > 0 ? tempArray[0] : "");
                    } else {
                        result.push(body[key]);
                    }
                }
                results.push(result);
                result = [];
            }
            console.log('Finished scraping...');
            resolve(results);
        } catch (error) {
            reject (error);
        }
    })
};

export default scrapeAmazonAsin;