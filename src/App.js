import MergeAmazonCSV from './js/MergeAmazonCSV.js';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <p>
          Amazon CSV Data Processor
        </p>
        <MergeAmazonCSV/>
      </header>
    </div>
  );
}

export default App;
